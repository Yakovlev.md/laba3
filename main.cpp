#include <iostream>
#include "progression.h"
#include "arithmetic.h"
#include "geometric.h"

double progression_sum(Progression &p, int n) {
    return p.sum(n);
};

int main() {
    int n;
    double a, d, b, r;

    std::cout << "Enter N =>";
    std::cin >> n;

    std::cout << "Enter first element and difference of arithmetic progression => ";
    std::cin >> a >> d;

    std::cout << "Enter first element and ratio of geometric progression => ";
    std::cin >> b >> r;

    Arithmetic p1(a, d);
    Geometric p2(b, r);

    std::cout << "Sum of elements in arithmetic progression = " << progression_sum(p1, n) << std::endl;
    std::cout << "Sum of elements in geomtric progression = " << progression_sum(p2, n) << std::endl;

    return 0;
}
