#include "arithmetic.h"

Arithmetic::Arithmetic(double first, double difference)
{
    this->first = first;
    this->difference = difference;
}

double Arithmetic::sum(int length)
{
    return (2 * first + difference * (length - 1)) * length / 2;
}

double Arithmetic::get_first()
{
    return this->first;
}

double Arithmetic::get_difference()
{
    return this->difference;
}

Arithmetic::~Arithmetic() {}
