#ifndef PROGRESSION_H
#define PROGRESSION_H


class Progression
{
public:
    virtual double sum(int length) = 0;

    virtual ~Progression() = 0;
};

#endif
